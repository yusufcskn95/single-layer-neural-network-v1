#pragma once
#include <time.h>
#include<cmath>
#define d1 1
#define d2 -1
#define Emax 0.01
#define euler 2.7182
#define c 0.1
double error = 1.0;
double *norm;
double *p;
double *q;
int aClass1 = 1;
int aClass2 = 1;
double w[3];
int dim1 = 0;
int dim2 = 0;
int cycle = 0;


namespace Project3 {
	
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Timers;	
	

	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			
			
			//
			//TODO: Add the constructor code here
			//
		}
		
	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::CheckBox^  class1;
	protected:
	private: System::Windows::Forms::CheckBox^  class2;
	private: System::Windows::Forms::PictureBox^  pictureBox1;
	private: System::Windows::Forms::Button^  rnd;
	private: System::Windows::Forms::Button^  normalize;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Button^  egit;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::CheckBox^  checkBox1;
	private: System::Windows::Forms::CheckBox^  checkBox2;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::GroupBox^  groupBox2;
	private: System::Windows::Forms::ComboBox^  comboBox1;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::ComboBox^  comboBox2;
	private: System::Windows::Forms::Label^  label5;





	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->class1 = (gcnew System::Windows::Forms::CheckBox());
			this->class2 = (gcnew System::Windows::Forms::CheckBox());
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->rnd = (gcnew System::Windows::Forms::Button());
			this->normalize = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->egit = (gcnew System::Windows::Forms::Button());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->checkBox1 = (gcnew System::Windows::Forms::CheckBox());
			this->checkBox2 = (gcnew System::Windows::Forms::CheckBox());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->comboBox2 = (gcnew System::Windows::Forms::ComboBox());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->comboBox1 = (gcnew System::Windows::Forms::ComboBox());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			this->groupBox1->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->SuspendLayout();
			// 
			// class1
			// 
			this->class1->AutoSize = true;
			this->class1->Location = System::Drawing::Point(32, 30);
			this->class1->Name = L"class1";
			this->class1->Size = System::Drawing::Size(57, 17);
			this->class1->TabIndex = 0;
			this->class1->Text = L"Class1";
			this->class1->UseVisualStyleBackColor = true;
			// 
			// class2
			// 
			this->class2->AutoSize = true;
			this->class2->Location = System::Drawing::Point(32, 93);
			this->class2->Name = L"class2";
			this->class2->Size = System::Drawing::Size(56, 17);
			this->class2->TabIndex = 1;
			this->class2->Text = L"class2";
			this->class2->UseVisualStyleBackColor = true;
			// 
			// pictureBox1
			// 
			this->pictureBox1->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->pictureBox1->Location = System::Drawing::Point(12, 12);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(400, 400);
			this->pictureBox1->TabIndex = 2;
			this->pictureBox1->TabStop = false;
			this->pictureBox1->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::pictureBox1_MouseClick);
			// 
			// rnd
			// 
			this->rnd->Location = System::Drawing::Point(32, 126);
			this->rnd->Name = L"rnd";
			this->rnd->Size = System::Drawing::Size(106, 31);
			this->rnd->TabIndex = 3;
			this->rnd->Text = L"Do�ru";
			this->rnd->UseVisualStyleBackColor = true;
			this->rnd->Click += gcnew System::EventHandler(this, &MyForm::rnd_Click);
			// 
			// normalize
			// 
			this->normalize->Location = System::Drawing::Point(32, 179);
			this->normalize->Name = L"normalize";
			this->normalize->Size = System::Drawing::Size(106, 37);
			this->normalize->TabIndex = 4;
			this->normalize->Text = L"Normalize Et";
			this->normalize->UseVisualStyleBackColor = true;
			this->normalize->Click += gcnew System::EventHandler(this, &MyForm::normalize_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(733, 306);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(27, 13);
			this->label1->TabIndex = 5;
			this->label1->Text = L"w[0]";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(733, 340);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(27, 13);
			this->label2->TabIndex = 6;
			this->label2->Text = L"w[1]";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(733, 374);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(27, 13);
			this->label3->TabIndex = 7;
			this->label3->Text = L"w[2]";
			// 
			// egit
			// 
			this->egit->Location = System::Drawing::Point(148, 126);
			this->egit->Name = L"egit";
			this->egit->Size = System::Drawing::Size(106, 31);
			this->egit->TabIndex = 8;
			this->egit->Text = L"Ayr�k E�itim";
			this->egit->UseVisualStyleBackColor = true;
			this->egit->Click += gcnew System::EventHandler(this, &MyForm::egit_Click);
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(733, 404);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(32, 13);
			this->label4->TabIndex = 9;
			this->label4->Text = L"cycle";
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(148, 182);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(106, 31);
			this->button1->TabIndex = 10;
			this->button1->Text = L"S�rekli E�itim";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &MyForm::surekli_egitim_Click);
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->rnd);
			this->groupBox1->Controls->Add(this->button1);
			this->groupBox1->Controls->Add(this->class1);
			this->groupBox1->Controls->Add(this->class2);
			this->groupBox1->Controls->Add(this->egit);
			this->groupBox1->Controls->Add(this->normalize);
			this->groupBox1->Location = System::Drawing::Point(723, 65);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(264, 238);
			this->groupBox1->TabIndex = 11;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"SingleClass";
			// 
			// checkBox1
			// 
			this->checkBox1->AutoSize = true;
			this->checkBox1->Location = System::Drawing::Point(619, 12);
			this->checkBox1->Name = L"checkBox1";
			this->checkBox1->Size = System::Drawing::Size(80, 17);
			this->checkBox1->TabIndex = 12;
			this->checkBox1->Text = L"SingleClass";
			this->checkBox1->UseVisualStyleBackColor = true;
			// 
			// checkBox2
			// 
			this->checkBox2->AutoSize = true;
			this->checkBox2->Location = System::Drawing::Point(521, 12);
			this->checkBox2->Name = L"checkBox2";
			this->checkBox2->Size = System::Drawing::Size(73, 17);
			this->checkBox2->TabIndex = 13;
			this->checkBox2->Text = L"MultiClass";
			this->checkBox2->UseVisualStyleBackColor = true;
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(706, 12);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(106, 31);
			this->button2->TabIndex = 14;
			this->button2->Text = L"Se�";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &MyForm::button2_Click);
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->label6);
			this->groupBox2->Controls->Add(this->comboBox2);
			this->groupBox2->Controls->Add(this->label5);
			this->groupBox2->Controls->Add(this->comboBox1);
			this->groupBox2->Location = System::Drawing::Point(442, 65);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(275, 238);
			this->groupBox2->TabIndex = 15;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"MultiClass";
			this->groupBox2->Enter += gcnew System::EventHandler(this, &MyForm::groupBox2_Enter);
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(128, 26);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(32, 13);
			this->label6->TabIndex = 18;
			this->label6->Text = L"Class";
			// 
			// comboBox2
			// 
			this->comboBox2->FormattingEnabled = true;
			this->comboBox2->Items->AddRange(gcnew cli::array< System::Object^  >(4) { L"3", L"4", L"5", L"6" });
			this->comboBox2->Location = System::Drawing::Point(131, 42);
			this->comboBox2->Name = L"comboBox2";
			this->comboBox2->Size = System::Drawing::Size(97, 21);
			this->comboBox2->TabIndex = 17;
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(5, 26);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(62, 13);
			this->label5->TabIndex = 16;
			this->label5->Text = L"Class Say�s�";
			// 
			// comboBox1
			// 
			this->comboBox1->FormattingEnabled = true;
			this->comboBox1->Items->AddRange(gcnew cli::array< System::Object^  >(4) { L"3", L"4", L"5", L"6" });
			this->comboBox1->Location = System::Drawing::Point(8, 42);
			this->comboBox1->Name = L"comboBox1";
			this->comboBox1->Size = System::Drawing::Size(97, 21);
			this->comboBox1->TabIndex = 16;
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(999, 436);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->checkBox2);
			this->Controls->Add(this->checkBox1);
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->pictureBox1);
			this->Name = L"MyForm";
			this->Text = L"MyForm";
			this->Load += gcnew System::EventHandler(this, &MyForm::MyForm_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	public: System::Void MyForm_Load(System::Object^  sender, System::EventArgs^  e) {

		groupBox1->Visible = false;
		groupBox2->Visible = false;
		
		
		Pen^ pen3 = gcnew Pen(Color::Black, 1.0f);



		pictureBox1->CreateGraphics()->DrawLine(pen3, 200, 0, 200, 400);
		pictureBox1->CreateGraphics()->DrawLine(pen3, 0, 200, 400, 200);

		
	}
	
	public: System::Void pictureBox1_MouseClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
	{
		
		Pen^ pen3 = gcnew Pen(Color::Black, 1.0f);		

		pictureBox1->CreateGraphics()->DrawLine(pen3, 200, 0, 200, 400);
		pictureBox1->CreateGraphics()->DrawLine(pen3, 0, 200, 400, 200);

		
		if (class1->Checked && class2->Checked) {
			MessageBox::Show("just one");
			class1->Checked = false;
			class2->Checked = true;
		}
		else 
		{
			if (class1->Checked || class2->Checked)
				if (class1->Checked) 
				{
					Pen^ pen = gcnew Pen(Color::Black, 3.0f);
					int temp_x, temp_y;
					
					temp_x = (System::Convert::ToInt32(e->X));
					temp_y = (System::Convert::ToInt32(e->Y));

					pictureBox1->CreateGraphics()->DrawLine(pen, temp_x - 5, temp_y, temp_x + 5, temp_y);
					pictureBox1->CreateGraphics()->DrawLine(pen, temp_x, temp_y - 5, temp_x, temp_y + 5);

					if (dim1==0)
					{						
						p = new double[3];
						p[0] =(double)temp_x - (pictureBox1->Width / 2);
						p[1] = (double)(pictureBox1->Height / 2) - temp_y;
						p[2] = 1.0;
						label1->Text = p[0].ToString();
						label2->Text = p[1].ToString();
						dim1++;						

					}
				
					else
					{	
						aClass1++;
						double *temp;
						temp = p;
						p = new double[aClass1 * 3];

						for (int i = 0; i < (aClass1-1)*3; i++)
						{
							p[i] = temp[i];
							
						}
						
						p[(aClass1-1)*3]= (double)temp_x - (pictureBox1->Width / 2);
						p[(aClass1-1)*3+1]= (double)(pictureBox1->Height / 2) - temp_y;
						p[(aClass1 - 1) * 3 + 2] = 1.0;
						label1->Text= p[(aClass1 - 1) * 3].ToString();
						label2->Text= p[(aClass1 - 1) * 3+1].ToString();
						delete temp;
					}

					
					
				}
				else 
				{
					Pen^ pen = gcnew Pen(Color::Red, 2.5f);
					int temp_x2, temp_y2;
					temp_x2 = (System::Convert::ToInt32(e->X));
					temp_y2 = (System::Convert::ToInt32(e->Y));

					pictureBox1->CreateGraphics()->DrawLine(pen, temp_x2 - 5, temp_y2, temp_x2 + 5, temp_y2);
					pictureBox1->CreateGraphics()->DrawLine(pen, temp_x2, temp_y2 - 5, temp_x2, temp_y2 + 5);
					
					if (dim2 == 0)
					{

						q = new double[3];
						q[0] = (double)temp_x2 - (pictureBox1->Width / 2);
						q[1] = (double)(pictureBox1->Height / 2) - temp_y2;
						q[2] = 1.0;
						label1->Text = q[0].ToString();
						label2->Text = q[1].ToString();
						
						dim2++;


					}

					else
					{
						aClass2++;
						double *temp;
						temp = q;
						q = new double[aClass2 * 3];

						for (int i = 0; i < (aClass2 - 1) * 3; i++)
						{
							q[i] = temp[i];

						}

						q[(aClass2 - 1) * 3] = (double)temp_x2 - (pictureBox1->Width / 2);
						q[(aClass2 - 1) * 3 + 1] = (double)(pictureBox1->Height / 2) - temp_y2;
						q[(aClass2 - 1) * 3 + 2] = 1.0;
						label1->Text = q[(aClass2 - 1) * 3].ToString();
						label2->Text = q[(aClass2 - 1) * 3 + 1].ToString();
						delete temp;
					}
				}
		}
	}
	public: System::Void rnd_Click(System::Object^  sender, System::EventArgs^  e) {

		
			Pen^ pen2 = gcnew Pen(Color::Blue, 1.5f);
			Random^ rnd = gcnew Random();

			w[0] = rnd->NextDouble(); label1->Text = w[0].ToString() + "x";
			w[1] = rnd->NextDouble(); label2->Text = w[1].ToString() + "y";
			w[2] = rnd->NextDouble(); label3->Text = w[2].ToString();
			
			float rx1 = pictureBox1->Width / 2;
			float ry1 = (0 - w[2] - rx1 * w[0]) / w[1];
			float rx2 = 0 - pictureBox1->Width / 2;
			float ry2 = (0 - w[2] - rx2 * w[0]) / w[1];

			pictureBox1->CreateGraphics()->DrawLine(pen2, rx1 + pictureBox1->Width / 2, pictureBox1->Height / 2 - ry1, rx2 + pictureBox1->Width / 2, pictureBox1->Height / 2 - ry2);
			pictureBox1->CreateGraphics()->DrawLine(pen2, rx1 + pictureBox1->Width / 2, pictureBox1->Height / 2 - ry1, rx2 + pictureBox1->Width / 2, pictureBox1->Height / 2 - ry2);
					
		
	}


	public: System::Void normalize_Click(System::Object^  sender, System::EventArgs^  e) {
		normFunction();
		double net;
		double fnet;

		while (error != 0)
		{
			dim1 = 0;
			error = 0;
			for (int i = 0; i < aClass1; i++)
			{
				net = 0;
				for (int j = 0; j < 3; j++)
					net += w[j] * norm[dim1 + j];

				if (net > 0)
					fnet = 1;

				else
					fnet = -1;

				for (int k = 0; k < 3; k++)
					w[k] = w[k] + (c *(d1 - fnet))*norm[dim1 + k];

				error += abs((d1 - fnet) / 2);

				drawAgain(0);
				dogruCiz(w);
				dim1 += 3;

			}

			//dim2 = 0;
			for (int i = aClass1; i < aClass2+aClass2; i++)
			{
				net = 0;
				for (int j = 0; j < 3; j++)
					net += w[j] * norm[dim1 + j];

				if (net > 0)
					fnet = 1;

				else
					fnet = -1;

				for (int k = 0; k < 3; k++)
					w[k] = w[k] + (c * (d2 - fnet))*norm[dim2 + k];

				error += abs((d2 - fnet) / 2);

				drawAgain(0);
				dogruCiz(w);
				dim1 += 3;

			}

			cycle++;

		}
		label1->Text = w[0].ToString() + "x";
		label1->Text = w[1].ToString() + "y";
		label3->Text = w[2].ToString();
		label4->Text = cycle.ToString() + " cycle";


}
			
	public: System::Void egit_Click(System::Object^  sender, System::EventArgs^  e) {		
		//burada classlar�n in t�m �rnekleri i�in net de�eri hesaplanacak.ve e�itim yap�lacak
		
		

		
		
		double net;
		double fnet;
			
		
		while (error!=0)
		{			
			dim1 = 0;
			error = 0;
			for (int i = 0; i < aClass1; i++)
			{
				net = 0;
				for (int j = 0; j < 3; j++)
					net += w[j] * p[dim1 + j];

				if (net > 0)
					fnet = 1;

				else
					fnet = -1;
	
				for (int k = 0; k < 3; k++)
					w[k] = w[k] + (c *(d1 - fnet))*p[dim1 + k];				
					
				error += abs((d1 - fnet) / 2);				
				
				drawAgain(1);
				dogruCiz(w);
				dim1 += 3;
				
			}

			dim2 = 0;
			for (int i = 0; i < aClass2; i++)
			{
				net = 0;
				for (int j = 0; j < 3; j++)
					net += w[j] * q[dim2 + j];

				if (net > 0)
					fnet = 1;

				else
					fnet = -1;

				for (int k = 0; k < 3; k++)
					w[k] = w[k] +( c * (d2 - fnet))*q[dim2 + k];
				
				error += abs((d2-fnet) / 2);				
				
				drawAgain(1);
				dogruCiz(w);				
				dim2 += 3;			
			
			}
			
			cycle++;

		}

		label1->Text = w[0].ToString() + "x";
		label1->Text = w[1].ToString() + "y";
		label3->Text = w[2].ToString();
		label4->Text = cycle.ToString()+" cycle";
	
}
			void dogruCiz(double *w)
			{
				//w[2] *= 10;
				if (w[1]!=0.0)
				
				{
					Pen^ pen2 = gcnew Pen(Color::Blue, 1.5f);
					float rx1 = pictureBox1->Width / 2;
					float ry1 = (0 - w[2] - rx1 * w[0]) / w[1];
					float rx2 = 0 - pictureBox1->Width / 2;
					float ry2 = (0 - w[2] - rx2 * w[0]) / w[1];
					pictureBox1->CreateGraphics()->DrawLine(pen2, rx1 + pictureBox1->Width / 2, pictureBox1->Height / 2 - ry1, rx2 + pictureBox1->Width / 2, pictureBox1->Height / 2 - ry2);
				}
				else
				{
					Pen^ pen2 = gcnew Pen(Color::Blue, 1.5f);
					float rx1 = (0 - w[2]) / w[0];//pictureBox1->Width / 2;
					float ry1 = 200;//(0 - w[2] - rx1 * w[0]) / w[1];
					float rx2 = (0 - w[2]) / w[0];
					float ry2 = -200;// (0 - w[2] - rx2 * w[0]) / w[1];
					pictureBox1->CreateGraphics()->DrawLine(pen2, rx1 + pictureBox1->Width / 2, pictureBox1->Height / 2 - ry1, rx2 + pictureBox1->Width / 2, pictureBox1->Height / 2 - ry2);
				}
				
			}

			void drawAgain(int a )
			{
				Pen^ pen1 = gcnew Pen(Color::Black, 2.5f);
				Pen^ pen = gcnew Pen(Color::Red, 2.5f);
				Pen^ pen3 = gcnew Pen(Color::Black, 1.0f);
				pictureBox1->Refresh();

				pictureBox1->CreateGraphics()->DrawLine(pen3, 200, 0, 200, 400);
				pictureBox1->CreateGraphics()->DrawLine(pen3, 0, 200, 400, 200);

				if (a == 1)
				{			


					for (int i = 0; i < aClass1 * 3; i += 3)
					{
						pictureBox1->CreateGraphics()->DrawLine(pen1, int(p[i] + pictureBox1->Width / 2 - 5), int(pictureBox1->Height / 2 - p[i + 1]), int(p[i] + pictureBox1->Width / 2 + 5), int(pictureBox1->Height / 2 - p[i + 1]));
						pictureBox1->CreateGraphics()->DrawLine(pen1, int(p[i] + pictureBox1->Width / 2), int(pictureBox1->Height / 2 - p[i + 1] - 5), int(p[i] + pictureBox1->Width / 2), int(pictureBox1->Height / 2 - p[i + 1] + 5));
					}

					for (int i = 0; i < aClass2 * 3; i += 3)
					{
						pictureBox1->CreateGraphics()->DrawLine(pen, int(q[i] + pictureBox1->Width / 2 - 5), int(pictureBox1->Height / 2 - q[i + 1]), int(q[i] + pictureBox1->Width / 2 + 5), int(pictureBox1->Height / 2 - q[i + 1]));
						pictureBox1->CreateGraphics()->DrawLine(pen, int(q[i] + pictureBox1->Width / 2), int(pictureBox1->Height / 2 - q[i + 1] - 5), int(q[i] + pictureBox1->Width / 2), int(pictureBox1->Height / 2 - q[i + 1] + 5));

					}

				}

				else
				{		
					for (int i = 0; i < aClass1*3; i+=3)
					{
						pictureBox1->CreateGraphics()->DrawLine(pen1, int((50 * norm[i]) + pictureBox1->Width / 2 - 5), int(pictureBox1->Height / 2 - (50 * norm[i + 1])), int((50 * norm[i]) + pictureBox1->Width / 2 + 5), int(pictureBox1->Height / 2 - (50 * norm[i + 1])));
						pictureBox1->CreateGraphics()->DrawLine(pen1, int((50 * norm[i]) + pictureBox1->Width / 2), int(pictureBox1->Height / 2 - (50 * norm[i + 1]) - 5), int((50 * norm[i]) + pictureBox1->Width / 2), int(pictureBox1->Height / 2 - (50* norm[i + 1]) + 5));
					}

					for (int i = (aClass1*3); i < (aClass1+aClass2)*3; i+=3)
					{
						pictureBox1->CreateGraphics()->DrawLine(pen, int((50 * norm[i]) + pictureBox1->Width / 2 - 5), int(pictureBox1->Height / 2 - (50 * norm[i + 1])), int((50 * norm[i]) + pictureBox1->Width / 2 + 5), int(pictureBox1->Height / 2 - (50 * norm[i + 1])));
						pictureBox1->CreateGraphics()->DrawLine(pen, int((50 * norm[i]) + pictureBox1->Width / 2), int(pictureBox1->Height / 2 - (50 * norm[i + 1]) - 5), int((50 * norm[i]) + pictureBox1->Width / 2), int(pictureBox1->Height / 2 - (50 * norm[i + 1]) + 5));

					}
					

				}
			}
			
			void normFunction()
			{
				double meanX;
				double meanY;
				double totalX;
				double totalY;
				double ssX;
				double ssY;
				int size = (aClass1 + aClass2) * 3;
				norm = new double[size];
				
				

				for (int i = 0; i <aClass1 ; i++)
				{
					meanX += p[i *3];					
					meanY += p[(i+1) * 3-2];					
										
				}

				for (int i = 0; i < aClass2; i++)
				{
					
					meanX += q[i + 3];					
					meanY += q[(i + 1) * 3 - 2];
				}


				meanX = meanX / (aClass1 + aClass2);
				meanY = meanY / (aClass1 + aClass2);

				for (int i = 0; i < aClass1; i++)
				{
					totalX += pow((p[i * 3] -   meanX),2);
					totalY += pow((p[(i+1) * 3-2] - meanY),2);

				}

				for (int i = 0; i < aClass2; i++)
				{
					totalX += pow((q[i * 3] - meanX), 2);
					totalY += pow((q[(i + 1) * 3 - 2] - meanY), 2);

				}
				

				ssX = sqrt(totalX / ((aClass1 + aClass2) - 1));
				ssY = sqrt(totalY / ((aClass1 + aClass2) - 1));

				
				for (int i = 0; i < aClass1; i++)
				{
					norm[i * 3] = (p[i * 3] - meanX) / ssX;
					norm[(i + 1) * 3 - 2] = (p[(i + 1) * 3 - 2] - meanY) / ssY;
					norm[(i + 1) * 3 - 1] = 1.0; //p[(i + 1) * 3 - 1];

				}

				for (int i = 0; i <aClass2; i++)
				{
					norm[(aClass1 * 3) + (i * 3)] = (q[i * 3] - meanX) / ssX;
					norm[(aClass1 * 3) + (i + 1) * 3 - 2] = (q[(i + 1) * 3 - 2] - meanY) / ssY;
					norm[(aClass1 * 3) + (i + 1) * 3 - 1] = -1.0; //q[(i + 1) * 3 - 1];

				}
				

			}


public: System::Void surekli_egitim_Click(System::Object^  sender, System::EventArgs^  e)
{

		double net;		
		double fnet;
		double fnetT�rev;
	
		normFunction();	
	
		do
		{
			dim1 = 0;
			error = 0;
			for (int i = 0; i < aClass1; i++)
			{

				//net hesapla w*x
				net = 0;
				for (int j = 0; j < 3; j++)
					net += w[j] * norm[dim1 + j];

				//fnet i hesapla
				fnet = 2 / (1 + pow(euler, (-1 * net))) - 1;

				//fnet in t�revini hesapla
				fnetT�rev = 0.5*(1 - (fnet * fnet));
				//fnetT�rev = (1 / (1 + pow(euler, -1 * net)))*(0 - pow(euler, -1 * net) / (1 + pow(euler, -1 * net)));

				//ag�rl�k g�ncellemesini yap
				for (int k = 0; k < 3; k++)
					w[k] = w[k] + c * (d1 - fnet)*fnetT�rev*norm[dim1 + k];

				error += 0.5*(pow((d1 - fnet), 2));

				drawAgain(0);
				dogruCiz(w);
				dim1 += 3;

			}
			error = error / (aClass1 + aClass2);


			//dim2 = 0;
			for (int i = aClass1; i < aClass1 + aClass2; i++)
			{
				//net hesapla w*x
				net = 0;
				for (int j = 0; j < 3; j++)
					net += w[j] * norm[dim1 + j];

				//fnet i hesapla
				fnet = 2 / (1 + pow(euler, (-1 * net))) - 1;

				//fnet in t�revini hesapla
				fnetT�rev = 0.5*(1 - (fnet * fnet));
				//fnetT�rev = (1 / (1 + pow(euler, -1 * net)))*(0 - pow(euler, -1 * net) / (1 + pow(euler, -1 * net)));

				//ag�rl�k g�ncellemesini yap
				for (int k = 0; k < 3; k++)
					w[k] = w[k] + c * (d2 - fnet)*fnetT�rev*norm[dim1 + k];

				error += 0.5*(pow((d2 - fnet), 2));

				drawAgain(0);
				dogruCiz(w);
				dim1 += 3;
				

			}

			error = error / (aClass1+aClass2);
			cycle++;

		} while (error >= Emax);

		label1->Text = w[0].ToString() + "x";
		label1->Text = w[1].ToString() + "y";
		label3->Text = w[2].ToString();
		label4->Text = cycle.ToString() + " cycle";	

		
		
}
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
	if (checkBox1->Checked)
	{
		groupBox1->Visible = true;
		groupBox2->Visible = false;
		checkBox1->Checked = false;

	}

	if (checkBox2->Checked)
	{
		groupBox2->Visible = true;
		groupBox1->Visible = false;
		checkBox2->Checked = false;
	}

}
private: System::Void groupBox2_Enter(System::Object^  sender, System::EventArgs^  e) {
}
};
}
